﻿using System;

namespace _IanYang_A2
{
    public class Game
    {
        private int[] pinFalls = new int[21];
        private int rollCounter;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public void Roll(int pins)
        {
            pinFalls[rollCounter] = pins;
            rollCounter++;
        }

        private bool IsStrike(int frameIndex)
        {
            return pinFalls[frameIndex] == 10;
        }

        private bool IsSpare(int frameIndex)
        {
            return pinFalls[frameIndex] + pinFalls[frameIndex + 1] == 10; 
        }

        private int StrikeBonus(int frameIndex)
        {
            return pinFalls[frameIndex + 1] + pinFalls[frameIndex + 2];
        }

        private int SpareBonus(int frameIndex)
        {
            return pinFalls[frameIndex + 2];
        }

        public int Score()
        {
            int score = 0;
            int frameIndex = 0;

            for (var frame = 0; frame < 10; frame++)
            {
                if (IsStrike(frameIndex))
                {
                    score += 10 + StrikeBonus(frameIndex);
                    frameIndex++;
                }
                else if (IsSpare(frameIndex))
                {
                    score += 10 + SpareBonus(frameIndex);
                    frameIndex += 2;
                }
                else
                {
                    score += pinFalls[frameIndex] + pinFalls[frameIndex + 1];
                    frameIndex += 2;
                }
            }

            return score;
        }
    }
}
