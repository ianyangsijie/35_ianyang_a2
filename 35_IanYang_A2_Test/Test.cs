﻿using NUnit.Framework;
using System;
using _IanYang_A2;

namespace _IanYang_A2_Test
{
    [TestFixture()]
    public class Test
    {
        private Game game;

        [SetUp]
        public void SetupGame()
        {
            game = new Game();
        }

        [Test]
        public void CreateGameObject()
        {
            game = new Game();
            Assert.That(game.GetType(), Is.EqualTo(typeof(Game)));
        }

        private void RollMany(int balls, int pins)
        {
            for (var i = 0; i < balls; i++) game.Roll(pins);
        }

        [Test]
        public void RollGutterGame()
        {
            RollMany(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }

        [Test]
        public void RollSpareFirstFrame()
        {
            game.Roll(9);
            game.Roll(1);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(29));
        }

        [Test]
        public void RollStrikeFirstFrame()
        {
            game.Roll(10);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(30));
        }

        [Test]
        public void RollPerfectGame()
        {
            RollMany(12, 10);
            Assert.That(game.Score(), Is.EqualTo(300));
        }

        [Test]
        public void RollSparesEveryFrame()
        {
            RollMany(21, 5);
            Assert.That(game.Score(), Is.EqualTo(150));
        }

        [Test]
        public void RollNineOneSpares()
        {
            for (var i = 0; i < 10; i++)
            {
                game.Roll(9);
                game.Roll(1);
            }
            game.Roll(9);
            Assert.That(game.Score(), Is.EqualTo(190));
        }

        [Test]
        public void RollTypicalGame()
        {
            game.Roll(10);
            game.Roll(9);
            game.Roll(1);
            game.Roll(5);
            game.Roll(5);
            game.Roll(7);
            game.Roll(2);
            game.Roll(10);
            game.Roll(10);
            game.Roll(10);
            game.Roll(9);
            game.Roll(0);
            game.Roll(8);
            game.Roll(2);
            game.Roll(9);
            game.Roll(1);
            game.Roll(10);
            Assert.That(game.Score(), Is.EqualTo(187));
        }
    }
}
